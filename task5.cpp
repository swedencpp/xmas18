
#include "doctest.h"
#include <vector>
#include <algorithm>


ptrdiff_t numbers_bigger100 (const std::vector<int>& vec) {
   // TODO, implement me
  return std::count_if(vec.begin(), vec.end(), [](const int& val) {
    return val > 100;
  });

  //return -1;
}


SCENARIO("looking for 'big' numbers") {

  GIVEN("a collection with 3 'big' numbers") {

    std::vector<int> vec = {2,140,60,103,10,99, 100, 234} ;

    WHEN("counting 'big' numbers") {

      auto answer = numbers_bigger100(vec) ;

      THEN("3 are found") {
        CHECK_EQ (answer, 3) ;
      }
    }
  }

  GIVEN("a an empty collection") {

    std::vector<int> vec  ;

    WHEN("counting 'big' numbers") {

      auto answer = numbers_bigger100(vec) ;

      THEN("there are none") {
        CHECK (answer == 0) ;
      }
    }
  }


}

