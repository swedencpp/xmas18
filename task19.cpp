#include "doctest.h"
#include <vector>
#include <algorithm>


void double_values(std::vector<int>& vec) {
  std::transform(vec.begin(), vec.end(),
   vec.begin(), [](int val){ return val*2 ;
  }) ;
}


SCENARIO("bring in order") {

  GIVEN("a collection with some elements") {

    std::vector<int> vec = {1,2,3,4,5} ;

    WHEN("double all the values") {

      double_values(vec) ;

      THEN("all the elements have double value") {
        CHECK (vec == std::vector<int>{2,4,6,8,10}) ;
      }
    }
  }


}

