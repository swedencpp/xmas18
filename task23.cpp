
#include "doctest.h"
#include <vector>
#include <algorithm>
#include <string>

std::string the_letters_of(const std::string& str1, const std::string& str2) {
// multiple statements required!
  auto str = str1 + str2 ;
  std::sort(str.begin(), str.end());
  str.erase(std::unique(str.begin(), str.end()), str.end());
  return str ;
}


SCENARIO("the_letters_of") {

  GIVEN("2 strings, ") {

    std::string str1 = "hello " ;
    std::string str2 = " world" ; ;

    WHEN("want to know the letters the two strings consists of") {

      auto str = the_letters_of(str1, str2) ;

      THEN("the we have a list of letters") {
        CHECK (str == " dehlorw") ;
      }
    }
  }
}

