#include "doctest.h"
#include <vector>
#include <algorithm>


void order(std::vector<int>& vec) {
  std::sort(vec.begin(), vec.end()) ;
}


SCENARIO("bring in order") {

  GIVEN("a collection of elements") {

    std::vector<int> vec = {3, 1, 5, 4, 2} ;

    WHEN("we bring in some order") {

      auto copy = vec;
      order(vec) ;

      THEN("the elements in the collection are in order") {
        CHECK (std::is_sorted(vec.begin(), vec.end())) ;
        REQUIRE( vec.size() == copy.size()) ;
        CHECK (std::is_permutation(vec.begin(), vec.end(),
                                   copy.begin() )) ;
      }
    }
  }
  GIVEN("a collection with no elements") {

      std::vector<int> vec{} ;

      WHEN("we bring in some order") {

        order(vec) ;

        THEN("there are still no elements in the collection") {
          CHECK (vec.empty()) ;
        }
      }
  }

}

