
#include "doctest.h"
#include <vector>
#include <algorithm>

bool operator < (const std::vector<int>& , const std::vector<int>& ) = delete;

//  point of this exercise is to not use the < operator

void order(std::vector<std::vector<int>>& vec) {
  std::sort(vec.begin(), vec.end(),
  [] (const std::vector<int>& a, const std::vector<int>& b) {
    return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end()) ;
  }) ;
}


SCENARIO("order vectors in a vector with your own words") {

  GIVEN("a vector of vectors with numbers in no particular order") {

    std::vector<std::vector<int>> vec;
    vec.push_back({3,2,1}) ;
    vec.push_back({3,2}) ;
    vec.push_back({1,1,1}) ;
    vec.push_back({1,2,1}) ;

    WHEN("we bring in some order") {

      order(vec) ;

      THEN("the outer vector contains the inner vectors in order") {
        CHECK_EQ ( vec.at(0), std::vector<int>{1,1,1} ) ;
        CHECK_EQ ( vec.at(1), std::vector<int>{1,2,1} ) ;
        CHECK_EQ ( vec.at(2), std::vector<int>{3,2} ) ;
        CHECK_EQ ( vec.at(3), std::vector<int>{3,2,1} ) ;
      }
    }
  }

}

