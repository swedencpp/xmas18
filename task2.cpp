
#include "doctest.h"
#include <vector>
#include <algorithm>

bool contains_the_meaning_of_life(const std::vector<int>& vec) {
  return std::find(vec.begin(), vec.end(),42) != vec.end() ;
}




SCENARIO("meaning of life") {

  GIVEN("a bunch of numbers, with with at least one number 42") {

    std::vector<int> vec = {7, 1,3,5,42, 6, 1} ;

    WHEN("looking for the meaning of life") {

      auto answer = contains_the_meaning_of_life(vec) ;

      THEN("it is found to be there") {
        CHECK (answer) ;

      }
    }
  }

  GIVEN("a bunch of numbers, none of which are 42") {

    std::vector<int> vec = {7, 1,3,5, 6, 1} ;

    WHEN("looking for the meaning of life") {

      auto answer = contains_the_meaning_of_life(vec) ;

      THEN("it cannot be found") {
        CHECK_FALSE (answer) ;

      }
    }
  }

}

