
#include "doctest.h"
#include <vector>
#include <algorithm>
#include <string>

void remove_leading_spaces( std::string& str) {

  str.erase(str.begin(),
            std::find_if_not(str.begin(), str.end(),
            [](char c){ return c == ' '; })) ;
}


SCENARIO("remove leading spaces") {

  GIVEN("2 strings, that starts with spaces") {

    std::string str1 = "   Hello " ;
    std::string str2 = " SwedenCpp::Stockholm" ;

    WHEN("removing the leading spaces") {

      remove_leading_spaces(str1) ;
      remove_leading_spaces(str2) ;

      THEN("the leading spaces are gone") {
        CHECK (str1 + str2 == "Hello SwedenCpp::Stockholm") ;

      }
    }
  }
}

