#include "doctest.h"
#include <vector>
#include <numeric>


int add_to_100(const std::vector<int>& vec) {
  return std::accumulate(vec.begin(), vec.end(), 100) ;
}


SCENARIO("adding numbers") {

  GIVEN("a collection with numbers 1,2,3,4,5") {

    std::vector<int> vec = {1,2,3,4,5} ;

    WHEN("we are adding them to 100") {

      auto answer = add_to_100(vec) ;

      THEN("we get 100+1+2+3+4+5") {
        CHECK_EQ (answer, 100+1+2+3+4+5) ;
      }
    }
  }
}

