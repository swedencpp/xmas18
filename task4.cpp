
#include "doctest.h"
#include <vector>
#include <algorithm>


bool all_even (const std::vector<int>& vec) {
  // TODO, implement me
  return std::all_of(vec.begin(), vec.end(), [](const int& val) {
    return val % 2 == 0;
  });

//  return false;
}



SCENARIO("looking for even numbers") {

  GIVEN("a collection with even numbers") {

    std::vector<int> vec = {-4, -2, 2,4,6,8,10,12} ;

    WHEN("testing at all numbers are even") {

      bool answer = all_even(vec) ;

      THEN("the the answer is yes") {
        CHECK (answer) ;
      }
    }
  }

  GIVEN("a collection with at least one odd number") {

    std::vector<int> vec = {16,-3, 2,4,6,8,11,12} ;

    WHEN("testing if all numbers are even") {

      bool answer = all_even(vec) ;

      THEN("the the answer is no") {
        CHECK_FALSE (answer) ;
      }
    }
  }


}

