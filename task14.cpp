#include "doctest.h"
#include <vector>
#include <algorithm>

void no_more_10s(std::vector<int>& vec) {
  std::replace(vec.begin(), vec.end(), 10, 0) ;
}


SCENARIO("no more tens") {

  GIVEN("a collection with some elements, where some are 10") {

    std::vector<int> vec = {3, 10, 5, 10, 4} ;

    WHEN("we do not want to have 10 but anything different") {

      no_more_10s(vec) ;

      THEN("there is no 10 anymore in the list") {
        for(const int i : vec) {
          CHECK_NE (i, 10) ;
        }
      }
      AND_THEN("the other elements are unchanged") {
        // and the rest is unchanged ;-) 
        CHECK_EQ (vec[0], 3) ;
        CHECK_EQ (vec[2], 5) ;
        CHECK_EQ (vec[4], 4) ;
      }
    }
  }


}

